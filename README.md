# README #

### What is this repository for? ###

Bezeqint Landing Pages. Build process that's supposed to automate the changes required between local and production environments.

### Dev guidelines ###
Fork or Clone the Repo.  
run yarn  
in config.js, change the values to match the current landing page: The path to the files in the server, and the URL for the page.  
  
work with: yarn build/watch (dist will be deleted before build).  
Eventually, the contents of dist/ should be deployed to production.  

If there are several html files, and each needs its own thanks page, set thanksEach to true in the config
  
##### Even if the design shows the phone with 2 fields, always do it with a single field. #####

### General ###
This build accomplishes the following:    
    
* Minify css and js.  
* Inserts the server_path from config to all paths in HTML.  
* Remove the top part of the html (wrapped in "build:devcss" comment). The deployed page is eventually injected into their own code, so it cant have tags like html,body,head etc.  
* Duplicate index.html to thanks.html. The only difference is giving the top_wrapper the class "thanks". Use this class to display the thank you message etc.  
  
You could give #top_wrapper the class .thanks and deisgn the thanks message. remove the class thanks when done.  
      
The file main.css is the large css file from Bezeqint's servers.  We include it in development to minimize css issues in production.
It's excluded from dist.

### FORM ###
I determine the environment based on the existance of "BI". the object exists only in production.
If we're local, the form will send to XPIM and then stop.  
Read the comments in the form. Some fields are predefined (e.g. custName), while others need to be defined using the "df.<name>" mechanism.  
  
### NOTES ###
Removed formmanager. Form now uses parsley.js:  
http://parsleyjs.org/  
Use the documentation for any customization.

##### Even if the design shows the phone with 2 fields, always do it with a single field. #####  
