var gulp = require('gulp');

config = require('./config');

require('./build/script')(gulp);
require('./build/libs')(gulp);
require('./build/sass')(gulp);
require('./build/images')(gulp);
require('./build/prodhtml')(gulp);


gulp.task('watch', ['libs:js','libs:css', 'sass', 'images', 'libs:fonts', 'prodhtml', 'prodthx', 'images:watch', 'sass:watch', 'scripts', 'scripts:watch', 'prodhtml:watch']);

gulp.task('build', ['scripts','libs:js','libs:css', 'images', 'sass', 'libs:fonts', 'prodhtml', 'prodthx']);
gulp.task('default', ['build']);