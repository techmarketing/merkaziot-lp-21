const gulp = require('gulp'),
    concat = require('gulp-concat'),
    util = require(  'gulp-util'),
    uglify = require('gulp-uglify'),
    replace = require('gulp-replace');

const path='src/scripts/**',
      jsDest='dist/scripts',
      jsSrc = 'src/js/';

module.exports = (gulp) => {

	gulp.task('scripts',function(){
        return gulp.src(path)
            .pipe(replace('//INITIALIZE-FORM', 'BIformInit();'))
            // .pipe(uglify())
            .pipe(gulp.dest(jsDest));
    });

    gulp.task('scripts:watch', () => {
        return gulp.watch(path, ['scripts']);
});
}


