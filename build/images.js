const gulp = require('gulp'),
    changed = require('gulp-changed'),
    imagemin = require('gulp-imagemin');

module.exports = (gulp) => {

    gulp.task('images', () => {
        return gulp.src('src/images/**/*')
            .pipe(changed('dist/images/'))
            .pipe(imagemin())
            .pipe(gulp.dest('dist/images/'));
    });

    gulp.task('images:watch', () => {
        return gulp.watch('src/images/**/*', ['images']);
    });

};